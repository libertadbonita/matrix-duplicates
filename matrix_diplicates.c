#include "stdio.h"
#define MAX_ELEMENTS 256
#define TRUE 1

int main(void)
{
    short newlineCount = 0;
    short duplicateExists = 0;

    short multiMode = 0;
    int multiAccumulator = 0;

    char currentChar;
    int elements[MAX_ELEMENTS];
    int elementIndex = 0;

    while (newlineCount < 2)
    {
	// get next character
	int status = scanf("%c", &currentChar);
	if (!status)
	{
	    printf("Error status [%d] on character [%c]\n", status, currentChar);
	}

	// Four cases are possible
	// 1. decimal character
	if (currentChar >= '0' && currentChar <= '9')
	{
	    newlineCount = 0;

	    int decimal = (int)(currentChar - '0');
	    if (multiMode)
	    {
		// add to accumulator
		multiAccumulator = (multiAccumulator * 10) + decimal;
	    } else
	    {
		// check for duplicates
		for (int i = 0; i < elementIndex; ++i)
		{
		    if (elements[i] == decimal)
		    {
			duplicateExists = TRUE;
		    }
		}
	      
		// add to elements
		if (elementIndex == MAX_ELEMENTS)
		{
		    printf("More than [%d] elements. Thats too many!", MAX_ELEMENTS);
		    return 1;
		}
		elements[elementIndex++] = decimal;
	    }
	}
      
	// 2. space
	else if (currentChar == ' ')
	{
	    // add accumulated value
	    if (multiMode)
	    {
		// check for duplicates
		for (int i = 0; i < elementIndex; ++i)
		{
		    if (elements[i] == multiAccumulator)
		    {
			duplicateExists = TRUE;
		    }
		}
	      
		// add to elements
		if (elementIndex == MAX_ELEMENTS)
		{
		    printf("More than [%d] elements. Thats too many!", MAX_ELEMENTS);
		    return 1;
		}
		elements[elementIndex++] = multiAccumulator;	      
	    }

	    // reset state
	    newlineCount = 0;
	    multiMode = TRUE;
	    multiAccumulator = 0;
	}

	// 3. newline
	else if (currentChar == '\n')
	{
	    // add accumulated value
	    if (multiMode)
	    {
		// check for duplicates
		for (int i = 0; i < elementIndex; ++i)
		{
		    if (elements[i] == multiAccumulator)
		    {
			duplicateExists = TRUE;
		    }
		}
	      
		// add to elements
		if (elementIndex == MAX_ELEMENTS)
		{
		    printf("More than [%d] elements. Thats too many!", MAX_ELEMENTS);
		    return 1;
		}
		elements[elementIndex++] = multiAccumulator;	      
	    }
	  
	    // reset state
	    newlineCount++;
	    multiMode = 0;
	}

	// 4. anything else
	else
	{
	    printf("Invalid character read [%c]\n", currentChar);
	    return 1;
	}
    }

    // Were there any duplicates?
    if (duplicateExists)
    {
	printf("yes");
    } else
    {
	printf("no");
    }

#ifdef DEBUG
    printf("\n[");
    for (int i = 0; i < elementIndex; ++i)
    {
	if (i == 0)
	{
	    printf("%d", elements[i]);
	}
	else
	{
	    printf(", %d", elements[i]);
	}
    }       
    printf("]\n");
#endif
  
  
    return 0;
}
