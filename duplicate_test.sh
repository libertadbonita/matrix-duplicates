#!/usr/bin/env bash

echo -e "1 2 3\n4 5 6\n"
./a.out <<EOF
1 2 3
4 5 6

EOF
echo

echo -e "1 2 3\n4 5 1\n"
./a.out <<EOF
1 2 3
4 5 1

EOF
echo

echo -e "123\n451\n"
./a.out <<EOF
123
451

EOF
echo

echo -e "1 12\n3 34\n"
./a.out <<EOF
1 12
3 34

EOF
echo

echo -e "1 12\n3 12\n"
./a.out <<EOF
1 12
3 12

EOF
echo

echo -e " 12\n 12\n"
./a.out <<EOF
 12
 12

EOF
echo
